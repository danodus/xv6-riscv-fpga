/*
top - Top level module implements:
- reset
- PLL
- SOC
*/
`default_nettype none
module top(
   input wire clk,
   input wire btn,
   input wire uart_rx,
   output wire uart_tx,
   output [5:0] led,
   input sd_miso,
   output sd_mosi,
   output sd_sck,
   output sd_ss,

   // "Magic" port names that the toolchain connects to the on-chip SDRAM
   output		O_sdram_clk,
   output		O_sdram_cke,
   output		O_sdram_cs_n,  // chip select
   output		O_sdram_cas_n, // columns address select
   output		O_sdram_ras_n, // row address select
   output		O_sdram_wen_n, // write enable
   inout [31:0]	IO_sdram_dq,       // 32 bit bidirectional data bus
   output [10:0]	O_sdram_addr,  // 11 bit multiplexed address bus
   output [1:0]	O_sdram_ba,    // two banks
   output [3:0]	O_sdram_dqm    // 32/4
);

//reset
reg [24:0]	rst_cnt = 0;
wire		rst = btn || !(& rst_cnt);
always @(posedge o_clk) begin
	rst_cnt <= rst_cnt + {23'd0,rst};
end

//PLL   
wire  o_clk;
pll PLL(.clock_in(clk),.clock_out(o_clk));
//SOC
soc SOC(
	.i_clk(o_clk),
	.i_rst(rst),
	.o_uart_tx(uart_tx),
	.i_uart_rx(uart_rx),
	.o_led(led),
	.i_spi_miso(sd_miso),
	.o_spi_mosi(sd_mosi),
	.o_spi_sck(sd_sck),
	.o_spi_ss(sd_ss),
  	.SDRAM_CLK(O_sdram_clk),        // Clock for SDRAM chip
 	.SDRAM_CKE(O_sdram_cke),        // Clock enabled
  	.SDRAM_D(IO_sdram_dq),          // Bidirectional data lines to/from SDRAM
  	.SDRAM_ADDR(O_sdram_addr),      // Address bus, multiplexed, 13 bits
  	.SDRAM_BA(O_sdram_ba),          // Bank select wires for 4 banks
  	.SDRAM_DQM(O_sdram_dqm),        // Byte mask
  	.SDRAM_CS(O_sdram_cs_n),        // Chip select
  	.SDRAM_WE(O_sdram_wen_n),       // Write enable
  	.SDRAM_RAS(O_sdram_ras_n),      // Row address select
  	.SDRAM_CAS(O_sdram_cas_n)       // Columns address select
);
   
endmodule
